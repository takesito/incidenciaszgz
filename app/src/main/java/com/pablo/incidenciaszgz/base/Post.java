package com.pablo.incidenciaszgz.base;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;


/**
 * Created by Pablo on 17/12/2017.
 */

public class Post {
    private int id;
    private String nombre;
    private String contenido;
    private Date fecha;

    public Post() {
        Calendar calendar = Calendar.getInstance();
        //SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm dd/MM/yyyy ");
        this.fecha =calendar.getTime();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String descripcion) {
        this.contenido = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}
