package com.pablo.incidenciaszgz.adapters;

import android.app.Activity;
import android.app.VoiceInteractor;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.pablo.incidenciaszgz.Comunidad;
import com.pablo.incidenciaszgz.R;
import com.pablo.incidenciaszgz.base.Incidencia;
import com.pablo.incidenciaszgz.base.Post;
import com.pablo.incidenciaszgz.util.Util;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Pablo on 17/12/2017.
 */
public class ComunidadAdapter extends ArrayAdapter<Post> {
    private Context contexto;
    private int layoutid;
    private ArrayList<Post> posts;


    public ComunidadAdapter(Context contexto, int layaoutId, ArrayList<Post> posts) {
        super(contexto,layaoutId,posts);
        this.contexto = contexto;
        this.posts = posts;
        this.layoutid = layaoutId;
    }

    static class ItemPost{
        TextView nombre;
        TextView contenido;
        TextView fecha;


    }

    @Override
    public View getView(int posicion, View view, ViewGroup padre) {

        View fila = view;
        ComunidadAdapter.ItemPost item = null;

        if (fila == null){
            LayoutInflater inflater = ((Activity)contexto).getLayoutInflater();
            fila = inflater.inflate(layoutid,padre,false);

            item = new ComunidadAdapter.ItemPost();
            item.nombre = (TextView) fila.findViewById(R.id.tvNombre);
            item.contenido = (TextView) fila.findViewById(R.id.tvContenido);
            item.fecha= (TextView) fila.findViewById(R.id.tvFecha);


            fila.setTag(item);
        }
        else {
            item = (ComunidadAdapter.ItemPost) fila.getTag();
        }

        Post post = posts.get(posicion);
        item.nombre.setText(post.getNombre());
        item.contenido.setText(post.getContenido());
        item.fecha.setText( Util.formatFechaHora(post.getFecha()));

        return fila;
    }
}
