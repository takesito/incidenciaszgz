package com.pablo.incidenciaszgz.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pablo.incidenciaszgz.R;
import com.pablo.incidenciaszgz.base.Favorito;
import com.pablo.incidenciaszgz.base.Incidencia;
import com.pablo.incidenciaszgz.util.Util;

import java.util.ArrayList;

/**
 * Created by Pablo on 13/12/2017.
 */
public class IncidenciaAdapter extends ArrayAdapter<Incidencia>{

    private Context contexto;
    private int layoutid;
    private ArrayList<Incidencia> incidencias;


    public IncidenciaAdapter(Context contexto, int layaoutId,ArrayList<Incidencia> incidencias) {
        super(contexto,layaoutId,incidencias);
        this.contexto=contexto;
        this.incidencias = incidencias;
        this.layoutid=layaoutId;
    }



    static class ItemIincidencia{
        ImageView icono;
        TextView titulo;
        TextView tramo;
        TextView fechainicio;
        TextView fechafin;

    }

    @Override
    public View getView(int posicion, View view, ViewGroup padre) {

        View fila = view;
        ItemIincidencia item = null;

        if (fila == null){
            LayoutInflater inflater = ((Activity)contexto).getLayoutInflater();
            fila = inflater.inflate(layoutid,padre,false);

            item = new ItemIincidencia();
            item.icono = (ImageView) fila.findViewById(R.id.ivIcono);
            item.titulo = (TextView) fila.findViewById(R.id.tvTitulo);
            item.tramo = (TextView) fila.findViewById(R.id.tvTramo);
            item.fechainicio = (TextView) fila.findViewById(R.id.tvFechaInicio);
            item.fechafin = (TextView) fila.findViewById(R.id.tvFechaFin);

            fila.setTag(item);
        }
        else {
            item = (ItemIincidencia) fila.getTag();
        }

        Incidencia incidencia = incidencias.get(posicion);
        item.titulo.setText(incidencia.getTitulo());
        item.tramo.setText(incidencia.getTramo());
        item.fechainicio.setText(Util.formatFecha(incidencia.getInicio()));
        item.fechafin.setText( Util.formatFecha(incidencia.getFin()));


        return fila;
    }
}
