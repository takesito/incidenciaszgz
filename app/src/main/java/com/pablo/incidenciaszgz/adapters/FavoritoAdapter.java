package com.pablo.incidenciaszgz.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pablo.incidenciaszgz.R;
import com.pablo.incidenciaszgz.base.Favorito;

import java.util.ArrayList;

/**
 * Created by Pablo on 13/12/2017.
 */
public class FavoritoAdapter extends ArrayAdapter<Favorito>{
    private Context contexto;
    private int layoutid;
    private ArrayList<Favorito> favoritos;


    public FavoritoAdapter(Context contexto, int layaoutId, ArrayList<Favorito> favoritos) {
        super(contexto,layaoutId,favoritos);
        this.contexto=contexto;
        this.favoritos = favoritos;
        this.layoutid=layaoutId;
    }

    static class ItemFavorito {
        ImageView icono;
        TextView titulo;
        TextView tramo;


    }

    @Override
    public View getView(int posicion, View view, ViewGroup padre) {

        View fila = view;
        ItemFavorito item = null;

        if (fila == null){
            LayoutInflater inflater = ((Activity)contexto).getLayoutInflater();
            fila = inflater.inflate(layoutid,padre,false);

            item = new ItemFavorito();
            item.icono = (ImageView) fila.findViewById(R.id.ivIconofav);
            item.titulo = (TextView) fila.findViewById(R.id.tvTitulofav);
            item.tramo = (TextView) fila.findViewById(R.id.tvTramofav);

            fila.setTag(item);
        }
        else {
            item = (ItemFavorito) fila.getTag();
        }

        Favorito favorito = favoritos.get(posicion);
        item.titulo.setText(favorito.getTitulo());
        item.tramo.setText(favorito.getTramo());



        return fila;
    }
}
