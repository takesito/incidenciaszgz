package com.pablo.incidenciaszgz.util;


import android.icu.text.SimpleDateFormat;

import java.text.ParseException;
import java.util.Date;

import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

/**
 * Created by Pablo on 13/12/2017.
 */

public class Util {
    public static LatLng DeUMTSaLatLng(double latitud, double longitud){
        UTMRef utm = new UTMRef(latitud, longitud,'N',30);
        return utm.toLatLng();
    }

    public static Date parseFecha(String fecha) throws ParseException {

        fecha = fecha.replace("T", " ");
        fecha = fecha.replace("Z", "");

        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-mm-dd HH:MM:SS");
        return sdf.parse(fecha);
    }

    public static String formatFecha(Date fecha)
    {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        return sdf.format(fecha);

    }

    public static String formatFechaHora(Date fecha)
    {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:MM:SS");

        return sdf.format(fecha);

    }



}
