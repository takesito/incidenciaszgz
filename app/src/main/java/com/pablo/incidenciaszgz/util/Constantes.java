package com.pablo.incidenciaszgz.util;

/**
 * Created by Pablo on 13/12/2017.
 */
public class Constantes {

    public final static long DURACION_SPLASH=3000;


    public static String URL ="https://www.zaragoza.es/sede/servicio/via-publica/incidencia.json";
    public static String URL_SERVIDOR = "http://192.168.1.41:8080";
    public final static String DATABASE_NAME="incidencias";
    public final static String TABLA_FAVORITOS="favoritos";
    public final static String ID_INCIDENCIA="id";
    public final static String TITULO_INCIDENCIA="titulo";
    public final static String TRAMO_INCIDENCIA="tramo";
    public final static String FECHA_INICIO_INCIDENCIA="fecha_inicio";
    public final static String FECHA_FIN_INCIDENCIA="fecha_fin";
    public final static String LATITUD_INCIDENCIA="latitud";
    public final static String LONGITUD_INCIDENCIA="longitud";
}
