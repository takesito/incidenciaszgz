package com.pablo.incidenciaszgz.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.pablo.incidenciaszgz.base.Favorito;
import com.pablo.incidenciaszgz.base.Incidencia;
import com.pablo.incidenciaszgz.util.Util;

import java.text.ParseException;
import java.util.ArrayList;

import static com.pablo.incidenciaszgz.util.Constantes.*;
import static com.pablo.incidenciaszgz.util.Constantes.TRAMO_INCIDENCIA;

/**
 * Created by Pablo on 16/12/2017.
 */

public class Database extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION=1;

    private static String[]CURSOR_FAVORITO={ID_INCIDENCIA,TITULO_INCIDENCIA,TRAMO_INCIDENCIA,FECHA_INICIO_INCIDENCIA,FECHA_FIN_INCIDENCIA,LATITUD_INCIDENCIA,LONGITUD_INCIDENCIA};
    private static String ORDER_FAV=TITULO_INCIDENCIA + " DESC";

    public Database(Context contexto){
        super(contexto,DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLA_FAVORITOS + " ("+ID_INCIDENCIA+ " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        TITULO_INCIDENCIA + " TEXT, "+
        TRAMO_INCIDENCIA + " TEXT, "+
        FECHA_INICIO_INCIDENCIA + " DATE, "+
        FECHA_FIN_INCIDENCIA + " DATE, "+
        LATITUD_INCIDENCIA + " FLOAT, "+
        LONGITUD_INCIDENCIA + " FLOAT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_FAVORITOS);
        onCreate(db);
    }

    public ArrayList<Favorito> getFavoritos(){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLA_FAVORITOS,CURSOR_FAVORITO,null,null,null,null,ORDER_FAV);

        ArrayList<Favorito> favoritos= new ArrayList<>();
        Favorito favorito = null;
        while (cursor.moveToNext()){
            favorito = new Favorito();
            favorito.setId(cursor.getInt(0));
            favorito.setTitulo(cursor.getString(1));
            favorito.setTramo(cursor.getString(2));
            favorito.setLatitud(cursor.getFloat(3));
            favorito.setLongitud(cursor.getFloat(4));

            favoritos.add(favorito);
        }
        db.close();
        return favoritos;

    }

    public void eliminarFavorito(Favorito favorito){
        SQLiteDatabase db=getWritableDatabase();
        String []argumentos = new String[]{String.valueOf(favorito.getId())};
        db.delete(TABLA_FAVORITOS,"id= ?",argumentos);
        db.close();
    }

    public void modificarFavorito(Favorito favorito, String nombre){
        SQLiteDatabase db=getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TITULO_INCIDENCIA,favorito.getTitulo());
        values.put(TRAMO_INCIDENCIA,favorito.getTramo());
        values.put(LATITUD_INCIDENCIA,favorito.getLatitud());
        values.put(LONGITUD_INCIDENCIA,favorito.getLongitud());

        String []argumentos = new String[]{nombre};
        db.update(TABLA_FAVORITOS,values,"titulo= ?",argumentos);
        db.close();
    }

    public void nuevoFavorito(Favorito favorito){
        SQLiteDatabase db = getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(TITULO_INCIDENCIA,favorito.getTitulo());
        values.put(TRAMO_INCIDENCIA,favorito.getTramo());
        values.put(LATITUD_INCIDENCIA,favorito.getLatitud());
        values.put(LONGITUD_INCIDENCIA,favorito.getLongitud());

        db.insertOrThrow(TABLA_FAVORITOS,null,values);
        db.close();
    }


}
