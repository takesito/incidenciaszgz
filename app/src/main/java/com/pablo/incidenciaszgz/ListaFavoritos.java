package com.pablo.incidenciaszgz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.pablo.incidenciaszgz.adapters.FavoritoAdapter;
import com.pablo.incidenciaszgz.base.Favorito;
import com.pablo.incidenciaszgz.database.Database;

import java.util.ArrayList;


import static com.pablo.incidenciaszgz.SplashScreen.theme;
import static com.pablo.incidenciaszgz.util.Constantes.LATITUD_INCIDENCIA;
import static com.pablo.incidenciaszgz.util.Constantes.LONGITUD_INCIDENCIA;
import static com.pablo.incidenciaszgz.util.Constantes.TITULO_INCIDENCIA;
import static com.pablo.incidenciaszgz.util.Constantes.TRAMO_INCIDENCIA;

public class ListaFavoritos extends AppCompatActivity {

    private ArrayList<Favorito> favoritos;
    private FavoritoAdapter adapter;

    private String titulo;
    private String tramo;
    private String inicio;
    private String fin;
    private Double latitud;
    private Double longitud;

    Database db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aplicarajustes();
        setContentView(R.layout.activity_lista_favoritos);

        setTitle(R.string.Favoritos);


        db= new Database(this);
        favoritos=db.getFavoritos();


        ListView lvFavoritos = (ListView) findViewById(R.id.lvFavoritos);


        adapter = new FavoritoAdapter(this,R.layout.favorito,favoritos);
        lvFavoritos.setAdapter(adapter);



        cargarListaFavoritos();

        registerForContextMenu(lvFavoritos);


    }

    private void cargarListaFavoritos() {

        titulo = getIntent().getStringExtra(TITULO_INCIDENCIA);
        tramo = getIntent().getStringExtra(TRAMO_INCIDENCIA);
        latitud = getIntent().getDoubleExtra(LATITUD_INCIDENCIA,-1);
        longitud = getIntent().getDoubleExtra(LONGITUD_INCIDENCIA,-1);

        if (titulo!=null){
            Favorito favorito = new Favorito();
            favorito.setTitulo(titulo);
            favorito.setTramo(tramo);
            favorito.setLatitud(latitud);
            favorito.setLongitud(longitud);
            db.nuevoFavorito(favorito);


            Intent intent = new Intent().setClass(ListaFavoritos.this,ListaFavoritos.class);

            startActivity(intent);


        }


    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        recargarFavoritos();
    }

    private void recargarFavoritos() {
        favoritos=db.getFavoritos();
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()){
            case R.id.lista:
                intent = new Intent().setClass(ListaFavoritos.this,ListaIncidencias.class);
                startActivity(intent);
                break;
            case R.id.favoritos:
                intent = new Intent().setClass(ListaFavoritos.this,ListaFavoritos.class);
                startActivity(intent);
                break;
            case R.id.acerca:
                intent = new Intent().setClass(ListaFavoritos.this,Acercade.class);
                startActivity(intent);
                break;
            case R.id.ajustes:
                intent = new Intent().setClass(ListaFavoritos.this,Ajustes.class);
                startActivity(intent);
                break;
            case R.id.mapa:
                intent = new Intent().setClass(ListaFavoritos.this,Mapa.class);
                startActivity(intent);
                break;
            case R.id.comunidad:
                intent = new Intent().setClass(ListaFavoritos.this,Comunidad.class);
                startActivity(intent);
                break;
            case R.id.codigo:
                intent = new Intent().setClass(ListaFavoritos.this,Codigo.class);
                startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.contextualfav,menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {


        AdapterView.AdapterContextMenuInfo info;
        info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final int itemseleccionado=info.position;
        Intent intent = null;
        final Favorito favorito = favoritos.get(itemseleccionado);
        switch (item.getItemId()){
            case R.id.lbModificar:
                intent = new Intent(ListaFavoritos.this,ModificarFavorito.class);
                intent.putExtra("titulo",favorito.getTitulo());
                startActivity(intent);
                return true;
            case R.id.lbEliminar:

                db.eliminarFavorito(favorito);
                intent = new Intent().setClass(ListaFavoritos.this,ListaFavoritos.class);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Favorito Eliminado",Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }

    public void aplicarajustes() {
        SharedPreferences sharedPreferences;
        sharedPreferences= getSharedPreferences("VALUES",MODE_PRIVATE);
        theme=sharedPreferences.getInt("THEME", 1);
        switch (theme){
            case 1: setTheme(R.style.AppTheme);
                break;
            case 2:setTheme(R.style.AppTheme2);
                break;
            case 3: setTheme(R.style.AppTheme3);
                break;
            case 4: setTheme(R.style.AppTheme4);
                break;
        }
    }

    @Override
    public void onBackPressed() {

    }
}
