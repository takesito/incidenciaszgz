package com.pablo.incidenciaszgz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static com.pablo.incidenciaszgz.SplashScreen.theme;
import static com.pablo.incidenciaszgz.util.Constantes.URL_SERVIDOR;

public class PublicarPost extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aplicarajustes();
        setContentView(R.layout.activity_publicar_post);

        Button btPublicar = (Button) findViewById(R.id.btPublicar);
        setTitle(" ");

        btPublicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Postear();
            }
        });

    }

    private void Postear() {
        EditText etNombrepost = (EditText) findViewById(R.id.etNombrepost);
        EditText etContenidopost = (EditText) findViewById(R.id.etContenidoPost);

        String nombre = etNombrepost.getText().toString();
        String contenido = etContenidopost.getText().toString();

        WebService webService = new WebService();
        webService.execute(nombre,contenido);

    }

    private class WebService extends AsyncTask<String,Void,Void>{
        private ProgressDialog dialog;
        @Override
        protected Void doInBackground(String... params) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getForObject(URL_SERVIDOR + "/add_post?nombre=" + params[0] + "&contenido=" + params[1],Void.class);

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(PublicarPost.this);
            dialog.setTitle(R.string.posteando);
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            EditText etNombre = (EditText) findViewById(R.id.etNombrepost);
            etNombre.setText("");
            EditText etContenido = (EditText) findViewById(R.id.etContenidoPost);
            etContenido.setText("");

            if (dialog != null)
                dialog.dismiss();

            Intent intent = new Intent().setClass(PublicarPost.this,Comunidad.class);
            startActivity(intent);
        }
    }
    public void aplicarajustes() {
        SharedPreferences sharedPreferences;
        sharedPreferences= getSharedPreferences("VALUES",MODE_PRIVATE);
        theme=sharedPreferences.getInt("THEME", 1);
        switch (theme){
            case 1: setTheme(R.style.AppTheme);
                break;
            case 2:setTheme(R.style.AppTheme2);
                break;
            case 3: setTheme(R.style.AppTheme3);
                break;
            case 4: setTheme(R.style.AppTheme4);
                break;
        }
    }

    @Override
    public void onBackPressed() {

    }
}
