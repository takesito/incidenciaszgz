package com.pablo.incidenciaszgz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import static com.pablo.incidenciaszgz.SplashScreen.theme;

public class Acercade extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acercade);
        aplicarajustes();

        setTitle(R.string.Acercade);
    }

    public void aplicarajustes() {
        SharedPreferences sharedPreferences;
        sharedPreferences= getSharedPreferences("VALUES",MODE_PRIVATE);
        theme=sharedPreferences.getInt("THEME", 1);
        switch (theme){
            case 1: setTheme(R.style.AppTheme);
                break;
            case 2:setTheme(R.style.AppTheme2);
                break;
            case 3: setTheme(R.style.AppTheme3);
                break;
            case 4: setTheme(R.style.AppTheme4);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()){
            case R.id.lista:
                intent = new Intent().setClass(Acercade.this,ListaIncidencias.class);
                startActivity(intent);
                break;
            case R.id.favoritos:
                intent = new Intent().setClass(Acercade.this,ListaFavoritos.class);
                startActivity(intent);
                break;
            case R.id.acerca:
                intent = new Intent().setClass(Acercade.this,Acercade.class);
                startActivity(intent);
                break;
            case R.id.ajustes:
                intent = new Intent().setClass(Acercade.this,Ajustes.class);
                startActivity(intent);
                break;
            case R.id.mapa:
                intent = new Intent().setClass(Acercade.this,Mapa.class);
                startActivity(intent);
                break;
            case R.id.comunidad:
                intent = new Intent().setClass(Acercade.this,Comunidad.class);
                startActivity(intent);
                break;
            case R.id.codigo:
                intent = new Intent().setClass(Acercade.this,Codigo.class);
                startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

    }
}
