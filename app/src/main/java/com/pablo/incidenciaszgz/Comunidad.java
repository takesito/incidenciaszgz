package com.pablo.incidenciaszgz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.fasterxml.jackson.core.util.ArraysCompat;
import com.pablo.incidenciaszgz.adapters.ComunidadAdapter;
import com.pablo.incidenciaszgz.base.Post;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;

import static com.pablo.incidenciaszgz.SplashScreen.theme;
import static com.pablo.incidenciaszgz.util.Constantes.URL_SERVIDOR;

public class Comunidad extends AppCompatActivity {


    private ArrayList<Post> posts;
    ComunidadAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aplicarajustes();
        setContentView(R.layout.activity_comunidad);


        setTitle(R.string.Comunidad);

        posts = new ArrayList<>();
        adapter = new ComunidadAdapter(this,R.layout.post,posts);

        Button btPostear = (Button) findViewById(R.id.btPostear);
        ListView lvPost = (ListView)findViewById(R.id.lvPost);
        lvPost.setAdapter(adapter);

        btPostear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Comunidad.this, PublicarPost.class);
                startActivity(intent);
            }
        });

        WebService service = new WebService();
        service.execute();

    }

    public void aplicarajustes() {
        SharedPreferences sharedPreferences;
        sharedPreferences= getSharedPreferences("VALUES",MODE_PRIVATE);
        theme=sharedPreferences.getInt("THEME", 1);
        switch (theme){
            case 1: setTheme(R.style.AppTheme);
                break;
            case 2:setTheme(R.style.AppTheme2);
                break;
            case 3: setTheme(R.style.AppTheme3);
                break;
            case 4: setTheme(R.style.AppTheme4);
                break;
        }
    }

    private class WebService extends AsyncTask<String,Void,Void>{

        private ProgressDialog dialog;
        @Override
        protected Void doInBackground(String... strings) {

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            Post[] postsArray = restTemplate.getForObject(URL_SERVIDOR + "/post", Post[].class);
            posts.addAll(Arrays.asList(postsArray));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapter.notifyDataSetChanged();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()){
            case R.id.lista:
                intent = new Intent().setClass(Comunidad.this,ListaIncidencias.class);
                startActivity(intent);
                break;
            case R.id.favoritos:
                intent = new Intent().setClass(Comunidad.this,ListaFavoritos.class);
                startActivity(intent);
                break;
            case R.id.acerca:
                intent = new Intent().setClass(Comunidad.this,Acercade.class);
                startActivity(intent);
                break;
            case R.id.ajustes:
                intent = new Intent().setClass(Comunidad.this,Ajustes.class);
                startActivity(intent);
                break;
            case R.id.mapa:
                intent = new Intent().setClass(Comunidad.this,Mapa.class);
                startActivity(intent);
                break;
            case R.id.comunidad:
                intent = new Intent().setClass(Comunidad.this,Comunidad.class);
                startActivity(intent);
                break;
            case R.id.codigo:
                intent = new Intent().setClass(Comunidad.this,Codigo.class);
                startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

    }

}
