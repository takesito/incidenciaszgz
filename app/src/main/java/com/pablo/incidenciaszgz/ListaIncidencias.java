package com.pablo.incidenciaszgz;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.Toast;

import com.pablo.incidenciaszgz.adapters.IncidenciaAdapter;
import com.pablo.incidenciaszgz.base.Incidencia;
import com.pablo.incidenciaszgz.util.Constantes;
import com.pablo.incidenciaszgz.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;

import static com.pablo.incidenciaszgz.SplashScreen.theme;
import static com.pablo.incidenciaszgz.util.Constantes.LATITUD_INCIDENCIA;
import static com.pablo.incidenciaszgz.util.Constantes.LONGITUD_INCIDENCIA;
import static com.pablo.incidenciaszgz.util.Constantes.TITULO_INCIDENCIA;
import static com.pablo.incidenciaszgz.util.Constantes.TRAMO_INCIDENCIA;

public class ListaIncidencias extends AppCompatActivity {

    private ArrayList<Incidencia> incidencias;
    private IncidenciaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aplicarajustes();
        setContentView(R.layout.activity_lista_incidencias);

        ListView lvIncidencias = (ListView) findViewById(R.id.lvIncidencias);
        incidencias = new ArrayList<>();
        adapter = new IncidenciaAdapter(this,R.layout.incidencia,incidencias);
        lvIncidencias.setAdapter(adapter);

        cargarListaIncidencias();

        registerForContextMenu(lvIncidencias);

        notificacion();

    }

    private void notificacion() {
        Intent intent = new Intent(this, PublicarPost.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

        Notification.Builder n  = new Notification.Builder(this)
                .setContentTitle("Precaucion amigo conductor")
                .setContentText("Algo ta pasando...postealo")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pIntent)
                .setAutoCancel(true);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(0, n.build());
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {


        AdapterContextMenuInfo info;
        info = (AdapterContextMenuInfo) item.getMenuInfo();
        final int itemseleccionado=info.position;

        switch (item.getItemId()){
            case R.id.lbfavorito:
                anadirfavorito(itemseleccionado);
                return true;
            case R.id.lbmapa:
                mostrarMapa(itemseleccionado);
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }

    private void mostrarMapa(int itemseleccionado) {
        final Incidencia incidencia = incidencias.get(itemseleccionado);

        Intent i = new Intent(ListaIncidencias.this,Mapa.class);
        i.putExtra(TITULO_INCIDENCIA,incidencia.getTitulo());
        i.putExtra(TRAMO_INCIDENCIA,incidencia.getTramo());
        i.putExtra(LATITUD_INCIDENCIA,incidencia.getLatitud());
        i.putExtra(LONGITUD_INCIDENCIA,incidencia.getLongitud());
        startActivity(i);
    }

    private void anadirfavorito(int itemseleccionado) {
        final Incidencia incidencia = incidencias.get(itemseleccionado);

        Toast.makeText(getApplicationContext(),"Añadido a favoritos", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(ListaIncidencias.this, ListaFavoritos.class);
        i.putExtra(TITULO_INCIDENCIA,incidencia.getTitulo());
        i.putExtra(TRAMO_INCIDENCIA,incidencia.getTramo());
        i.putExtra(LATITUD_INCIDENCIA,incidencia.getLatitud());
        i.putExtra(LONGITUD_INCIDENCIA,incidencia.getLongitud());
        startActivity(i);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.contextual,menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()){
            case R.id.lista:
                intent = new Intent().setClass(ListaIncidencias.this,ListaIncidencias.class);
                startActivity(intent);
                break;
            case R.id.favoritos:
                intent = new Intent().setClass(ListaIncidencias.this,ListaFavoritos.class);
                startActivity(intent);
                break;
            case R.id.acerca:
                intent = new Intent().setClass(ListaIncidencias.this,Acercade.class);
                startActivity(intent);
                break;
            case R.id.ajustes:
                intent = new Intent().setClass(ListaIncidencias.this,Ajustes.class);
                startActivity(intent);
                break;
            case R.id.mapa:
                intent = new Intent().setClass(ListaIncidencias.this,Mapa.class);
                startActivity(intent);
                break;
            case R.id.comunidad:
                intent = new Intent().setClass(ListaIncidencias.this,Comunidad.class);
                startActivity(intent);
                break;
            case R.id.codigo:
                intent = new Intent().setClass(ListaIncidencias.this,Codigo.class);
                startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public class TareaDescarga extends AsyncTask<String, Void, Void> {

        private boolean error=false;



        @Override
        protected Void doInBackground(String... urls) {

            try {
                URL url = new URL(Constantes.URL);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String linea = null;

                while ((linea = br.readLine()) != null)
                    sb.append(linea + "\n");

                conexion.disconnect();
                br.close();
                String resultado = sb.toString();

                JSONObject json = new JSONObject(resultado);
                JSONArray jsonArray = json.getJSONArray("result");

                String id, titulo, tramo;
                String coordenadas;
                String fechaInicio, fechaFin;

                Incidencia incidencia = null;
                for (int i = 0; i < jsonArray.length();i++){
                    id = jsonArray.getJSONObject(i).getString("id");
                    titulo = jsonArray.getJSONObject(i).getString("title");
                    tramo = jsonArray.getJSONObject(i).getString("tramo");
                    fechaInicio = jsonArray.getJSONObject(i).getString("inicio");
                    fechaFin = jsonArray.getJSONObject(i).getString("fin");

                    coordenadas = jsonArray.getJSONObject(i).getJSONObject("geometry").getString("coordinates");
                    coordenadas = coordenadas.substring(1,coordenadas.length()-1);
                    String latlong[]= coordenadas.split(",");
                    uk.me.jstott.jcoord.LatLng latLng = Util.DeUMTSaLatLng(Double.parseDouble(latlong[0]),Double.parseDouble(latlong[1]));

                    incidencia = new Incidencia();
                    incidencia.setId(Integer.parseInt(id));
                    incidencia.setTitulo(titulo);
                    incidencia.setTramo(tramo);
                    incidencia.setLatitud(latLng.getLat());
                    incidencia.setLongitud(latLng.getLng());
                    incidencia.setInicio(Util.parseFecha(fechaInicio));
                    incidencia.setFin(Util.parseFecha(fechaFin));
                    incidencias.add(incidencia);

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Void resultado) {
            super.onPostExecute(resultado);

            if (error){
                Toast.makeText(getApplicationContext(),"error",Toast.LENGTH_SHORT).show();
                return;
            }
            adapter.notifyDataSetChanged();

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            adapter.clear();
            incidencias= new ArrayList<>();


        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);


            adapter.notifyDataSetChanged();

        }
    }

    private void cargarListaIncidencias(){
        TareaDescarga tarea = new TareaDescarga();
        tarea.execute();
    }

    public void aplicarajustes() {
        SharedPreferences sharedPreferences;
        sharedPreferences= getSharedPreferences("VALUES",MODE_PRIVATE);
        theme=sharedPreferences.getInt("THEME", 1);
        switch (theme){
            case 1: setTheme(R.style.AppTheme);
                break;
            case 2:setTheme(R.style.AppTheme2);
                break;
            case 3: setTheme(R.style.AppTheme3);
                break;
            case 4: setTheme(R.style.AppTheme4);
                break;
        }
    }

    @Override
    public void onBackPressed() {

    }
}
