package com.pablo.incidenciaszgz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pablo.incidenciaszgz.base.Favorito;
import com.pablo.incidenciaszgz.database.Database;

import static com.pablo.incidenciaszgz.SplashScreen.theme;
import static com.pablo.incidenciaszgz.util.Constantes.TITULO_INCIDENCIA;

public class ModificarFavorito extends AppCompatActivity {

    private Button btmodificar;
    private EditText etNombremod;
    private EditText etTramoMod;

    Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aplicarajustes();
        setContentView(R.layout.activity_modificar_favorito);
        setTitle(" ");


        etNombremod = (EditText) findViewById(R.id.etNombremod);
        etTramoMod = (EditText) findViewById(R.id.etTramoMod);
        db=new Database(this);

        btmodificar= (Button) findViewById(R.id.btModificar);
        btmodificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                String nombre = intent.getStringExtra(TITULO_INCIDENCIA);

                Favorito modFavorito = new Favorito();
                modFavorito.getId();
                modFavorito.setTitulo(String.valueOf(etNombremod.getText()));
                modFavorito.setTramo(String.valueOf(etTramoMod.getText()));
                modFavorito.getLatitud();
                modFavorito.getLongitud();

                db.modificarFavorito(modFavorito,nombre);
                intent = new Intent().setClass(ModificarFavorito.this,ListaFavoritos.class);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Favorito Modificado",Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void aplicarajustes() {
        SharedPreferences sharedPreferences;
        sharedPreferences= getSharedPreferences("VALUES",MODE_PRIVATE);
        theme=sharedPreferences.getInt("THEME", 1);
        switch (theme){
            case 1: setTheme(R.style.AppTheme);
                break;
            case 2:setTheme(R.style.AppTheme2);
                break;
            case 3: setTheme(R.style.AppTheme3);
                break;
            case 4: setTheme(R.style.AppTheme4);
                break;
        }
    }

    @Override
    public void onBackPressed() {

    }
}
