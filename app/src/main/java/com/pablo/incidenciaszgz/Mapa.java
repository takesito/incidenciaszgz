package com.pablo.incidenciaszgz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mapbox.mapboxsdk.MapboxAccountManager;

import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationServices;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.services.Constants;
import com.mapbox.services.commons.ServicesException;
import com.mapbox.services.commons.geojson.LineString;
import com.mapbox.services.commons.models.Position;
import com.mapbox.services.directions.v5.DirectionsCriteria;
import com.mapbox.services.directions.v5.MapboxDirections;
import com.mapbox.services.directions.v5.models.DirectionsResponse;
import com.mapbox.services.directions.v5.models.DirectionsRoute;
import com.pablo.incidenciaszgz.util.Util;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.pablo.incidenciaszgz.SplashScreen.theme;

public class Mapa extends AppCompatActivity {

    private MapView mapView;
    private double latitud;
    private double longitud;
    private String titulo;
    private String tramo;

    private MapboxMap mapabox;
    private FloatingActionButton floatUbicacion;
    private LocationServices servicioUbicacion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aplicarajustes();
        MapboxAccountManager.start(this,"pk.eyJ1IjoidGFrZXNpdG8iLCJhIjoiY2piY2NmOWQ3MTM1OTJ2b2YwNmI0dTF4NyJ9._djOau-9JwSgzW0yvDGe_Q");
        setContentView(R.layout.activity_mapa);
        setTitle(R.string.lb_mapa);
        mapView = (MapView) findViewById(R.id.mapa);
        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapabox=mapboxMap;
            }
        });

        latitud = getIntent().getDoubleExtra("latitud",-1);
        longitud = getIntent().getDoubleExtra("longitud",-1);
        titulo = getIntent().getStringExtra("titulo");
        tramo = getIntent().getStringExtra("tramo");



        ubicarIncidencia();
    }

    private void ubicarIncidencia() {
        servicioUbicacion = LocationServices.getLocationServices(this);
        floatUbicacion = (FloatingActionButton) findViewById(R.id.floatUbicacion);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapboxMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitud, longitud))
                        .title(titulo)
                        .snippet(tramo)
                        );
                CameraPosition position = new CameraPosition.Builder()
                        .target(new LatLng(latitud, longitud)) // Fija la posición
                        .zoom(17) // Fija el nivel de zoom
                        .tilt(30) // Fija la inclinación de la cámara
                        .build();

                mapboxMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(position), 7000);
            }
        });




        floatUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mapabox != null){
                    Location userLocation = servicioUbicacion.getLastLocation();
                    if (userLocation != null)
                        mapabox.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(userLocation),16));
                    mapabox.setMyLocationEnabled(true);

                    try {
                        obtenerRuta(longitud,latitud,userLocation);

                    } catch (ServicesException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void obtenerRuta(double longitud, double latitud, Location userLocation) throws ServicesException {

        Position posicionMarker = Position.fromCoordinates(longitud,latitud);
        Position posicionUsuario = Position.fromCoordinates(userLocation.getLongitude(),userLocation.getLatitude());

        MapboxDirections direccion = new MapboxDirections.Builder()
                .setOrigin(posicionMarker)
                .setDestination(posicionUsuario)
                .setProfile(DirectionsCriteria.PROFILE_DRIVING)
                .setAccessToken(MapboxAccountManager.getInstance().getAccessToken())
                .build();
        direccion.enqueueCall(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                DirectionsRoute ruta = response.body().getRoutes().get(0);
                Toast.makeText(Mapa.this,"Distancia: "+ruta.getDistance()+" metros",Toast.LENGTH_SHORT).show();
                pintarRuta(ruta);
            }

            @Override
            public void onFailure(Call<DirectionsResponse> call, Throwable t) {
                Toast.makeText(Mapa.this,"Error en el calculo de ruta",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void pintarRuta(DirectionsRoute ruta) {
        LineString lineString = LineString.fromPolyline(ruta.getGeometry(), Constants.OSRM_PRECISION_V5);
        List<Position> coordenadas = lineString.getCoordinates();
        LatLng[]puntos = new  LatLng[coordenadas.size()];
        for (int i=0;i<coordenadas.size();i++){
            puntos[i] = new LatLng(coordenadas.get(i).getLatitude(),coordenadas.get(i).getLongitude());
        }
        mapabox.addPolyline(new PolylineOptions()
        .add(puntos)
        .color(Color.parseColor("#009688"))
        .width(5));
        if (!mapabox.isMyLocationEnabled())
            mapabox.setMyLocationEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()){
            case R.id.lista:
                intent = new Intent().setClass(Mapa.this,ListaIncidencias.class);
                startActivity(intent);
                break;
            case R.id.favoritos:
                intent = new Intent().setClass(Mapa.this,ListaFavoritos.class);
                startActivity(intent);
                break;
            case R.id.acerca:
                intent = new Intent().setClass(Mapa.this,Acercade.class);
                startActivity(intent);
                break;
            case R.id.ajustes:
                intent = new Intent().setClass(Mapa.this,Ajustes.class);
                startActivity(intent);
                break;
            case R.id.mapa:
                intent = new Intent().setClass(Mapa.this,Mapa.class);
                startActivity(intent);
                break;
            case R.id.comunidad:
                intent = new Intent().setClass(Mapa.this,Comunidad.class);
                startActivity(intent);
                break;
            case R.id.codigo:
                intent = new Intent().setClass(Mapa.this,Codigo.class);
                startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void aplicarajustes() {
        SharedPreferences sharedPreferences;
        sharedPreferences= getSharedPreferences("VALUES",MODE_PRIVATE);
        theme=sharedPreferences.getInt("THEME", 1);
        switch (theme){
            case 1: setTheme(R.style.AppTheme);
                break;
            case 2:setTheme(R.style.AppTheme2);
                break;
            case 3: setTheme(R.style.AppTheme3);
                break;
            case 4: setTheme(R.style.AppTheme4);
                break;
        }
    }

    @Override
    public void onBackPressed() {

    }
}
