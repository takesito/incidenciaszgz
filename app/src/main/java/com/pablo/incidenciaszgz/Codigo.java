package com.pablo.incidenciaszgz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.pablo.incidenciaszgz.util.IntentIntegrator;
import com.pablo.incidenciaszgz.util.IntentResult;

import static com.pablo.incidenciaszgz.SplashScreen.theme;

public class Codigo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aplicarajustes();
        setContentView(R.layout.activity_codigo);

        setTitle(R.string.CodigoQR);

        configureButtonReader();
    }

    public void aplicarajustes() {
        SharedPreferences sharedPreferences;
        sharedPreferences= getSharedPreferences("VALUES",MODE_PRIVATE);
        theme=sharedPreferences.getInt("THEME", 1);
        switch (theme){
            case 1: setTheme(R.style.AppTheme);
                break;
            case 2:setTheme(R.style.AppTheme2);
                break;
            case 3: setTheme(R.style.AppTheme3);
                break;
            case 4: setTheme(R.style.AppTheme4);
                break;
        }
    }

    private void configureButtonReader() {
        final ImageButton buttonReader = (ImageButton) findViewById(R.id.imageButton);
        buttonReader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new IntentIntegrator(Codigo.this).initiateScan();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        final IntentResult scanresult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        handleResult(scanresult);

    }

    private void handleResult(IntentResult scanresult) {
        if (scanresult != null){
            updateUITextViews(scanresult.getContents(),scanresult.getFormatName());
        } else {
            Toast.makeText(this, R.string.Leererror,Toast.LENGTH_SHORT).show();
        }
    }

    private void updateUITextViews(String contents, String formatName) {
        ((TextView)findViewById(R.id.tvResultado)).setText(formatName);
        final TextView tvResult = (TextView) findViewById(R.id.tvResultado);
        tvResult.setText(contents);
        Linkify.addLinks(tvResult,Linkify.ALL);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()){
            case R.id.lista:
                intent = new Intent().setClass(Codigo.this,ListaIncidencias.class);
                startActivity(intent);
                break;
            case R.id.favoritos:
                intent = new Intent().setClass(Codigo.this,ListaFavoritos.class);
                startActivity(intent);
                break;
            case R.id.acerca:
                intent = new Intent().setClass(Codigo.this,Acercade.class);
                startActivity(intent);
                break;
            case R.id.ajustes:
                intent = new Intent().setClass(Codigo.this,Ajustes.class);
                startActivity(intent);
                break;
            case R.id.mapa:
                intent = new Intent().setClass(Codigo.this,Mapa.class);
                startActivity(intent);
                break;
            case R.id.comunidad:
                intent = new Intent().setClass(Codigo.this,Comunidad.class);
                startActivity(intent);
                break;
            case R.id.codigo:
                intent = new Intent().setClass(Codigo.this,Codigo.class);
                startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

    }
}
