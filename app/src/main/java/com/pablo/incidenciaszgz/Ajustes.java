package com.pablo.incidenciaszgz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import static com.pablo.incidenciaszgz.SplashScreen.theme;


import static com.pablo.incidenciaszgz.util.Constantes.URL_SERVIDOR;

public class Ajustes extends AppCompatActivity {

    Button btRojo;
    Button btAzul;
    Button btVerde;
    Button btNaranja;

    Button btEspanol;
    Button btIngles;


    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aplicarajustes();



        setContentView(R.layout.activity_ajustes);
        setTitle(R.string.Ajustes);


        Button btAplicar = (Button) findViewById(R.id.btAplicar);
        final EditText etIP = (EditText) findViewById(R.id.etIP);


        btAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                URL_SERVIDOR="http://"+etIP.getText().toString()+":8080";
                System.out.println("SERVIDOR CAMBIADO " + URL_SERVIDOR);
            }
        });




        btRojo= (Button) findViewById(R.id.btRojo);
        btRojo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPreferences.edit().putInt("THEME",1).apply();
                theme=1;
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.rojoMensaje),Toast.LENGTH_SHORT).show();
                Intent intent=new Intent().setClass(
                        Ajustes.this,Ajustes.class);
                startActivity(intent);
            }
        });
        btAzul= (Button) findViewById(R.id.btAzul);
        btAzul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPreferences.edit().putInt("THEME",2).apply();
                theme=2;
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.azulMensaje),Toast.LENGTH_SHORT).show();
                Intent intent=new Intent().setClass(
                        Ajustes.this,Ajustes.class);
                startActivity(intent);
            }
        });
        btVerde = (Button) findViewById(R.id.btVerde);
        btVerde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPreferences.edit().putInt("THEME",3).apply();
                theme=3;Toast.makeText(getApplicationContext(),getResources().getString(R.string.verdeMensaje),Toast.LENGTH_SHORT).show();

                Intent intent=new Intent().setClass(
                        Ajustes.this,Ajustes.class);
                startActivity(intent);
            }
        });
        btNaranja = (Button) findViewById(R.id.btNaranja);
        btNaranja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPreferences.edit().putInt("THEME",4).apply();
                theme=4;
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.naranjaMensaje),Toast.LENGTH_SHORT).show();
                Intent intent=new Intent().setClass(
                        Ajustes.this,Ajustes.class);
                startActivity(intent);
            }
        });


        btEspanol= (Button) findViewById(R.id.btEspanol);
        btEspanol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Locale localizacion = new Locale("es", "ES");

                Locale.setDefault(localizacion);
                Configuration config = new Configuration();
                config.locale = localizacion;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.espanyolMensaje), Toast.LENGTH_SHORT).show();
                Intent intent=new Intent().setClass(
                        Ajustes.this,Ajustes.class);
                startActivity(intent);

            }
        });
        btIngles= (Button) findViewById(R.id.btIngles);
        btIngles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Locale localizacion = new Locale("en", "EN");

                Locale.setDefault(localizacion);
                Configuration config = new Configuration();
                config.locale = localizacion;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.englishMessage), Toast.LENGTH_SHORT).show();
                Intent intent=new Intent().setClass(
                        Ajustes.this,Ajustes.class);
                startActivity(intent);
            }
        });



    }

    public void aplicarajustes() {
        sharedPreferences=getSharedPreferences("VALUES",MODE_PRIVATE);
        theme=sharedPreferences.getInt("THEME", 1);
        switch (theme){
            case 1: setTheme(R.style.AppTheme);
                break;
            case 2:setTheme(R.style.AppTheme2);
                break;
            case 3: setTheme(R.style.AppTheme3);
                break;
            case 4: setTheme(R.style.AppTheme4);
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()){
            case R.id.lista:
                intent = new Intent().setClass(Ajustes.this,ListaIncidencias.class);
                startActivity(intent);
                break;
            case R.id.favoritos:
                intent = new Intent().setClass(Ajustes.this,ListaFavoritos.class);
                startActivity(intent);
                break;
            case R.id.acerca:
                intent = new Intent().setClass(Ajustes.this,Acercade.class);
                startActivity(intent);
                break;
            case R.id.ajustes:
                intent = new Intent().setClass(Ajustes.this,Ajustes.class);
                startActivity(intent);
                break;
            case R.id.mapa:
                intent = new Intent().setClass(Ajustes.this,Mapa.class);
                startActivity(intent);
                break;
            case R.id.comunidad:
                intent = new Intent().setClass(Ajustes.this,Comunidad.class);
                startActivity(intent);
                break;
            case R.id.codigo:
                intent = new Intent().setClass(Ajustes.this,Codigo.class);
                startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

    }
}
